Name:           gcc-for-openEuler
Version:        1.0.4
Release:        6
Summary:        GCC released as a binary package for openEuler and other OSes.

License:        GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2 and BSD
URL:            https://gitee.com/openEuler/gcc-for-openEuler
Source0:        %{name}-%{version}.tar.gz

%global config config.xml
%global build_name gcc-10.3.1-2022.12-aarch64-linux

ExclusiveArch:	aarch64

BuildRequires:  git bison flex texinfo autoconf libtool elfutils-libelf-devel glibc-static
BuildRequires:	chrpath centos-release-scl devtoolset-7 python3 zlib-devel lksctp-tools-devel

%description
GCC released as a binary package for openEuler and other OSes.

%skip debuginfo packages
%global debug_package %{nil}

%prep
%autosetup
cp -f %{_sourcedir}/%{config} build/

%build
cd %{_builddir}/%{name}-%{version}/build
bash download.sh
bash build.sh hcc_arm64le_native

%install
cd %{buildroot}
cp %{_builddir}/%{name}-%{version}/output/%{build_name}/%{build_name}.tar.gz .
touch %{build_name}.tar.gz.sha256
sha256sum %{build_name}.tar.gz > %{build_name}.tar.gz.sha256

%files
%attr(755, root, root) /%{build_name}.tar.gz
%attr(755, root, root) /%{build_name}.tar.gz.sha256

%changelog
* Wed Dec 21 2022 Xiong Zhou <xiongzhou4@huawei.com> - 1.0.4-6
- Update the date to 2022.12 in the gcc build name.

* Wed Nov 16 2022 Xiong Zhou <xiongzhou4@huawei.com> - 1.0.4-5
- Update the date in the gcc build name.

* Tue Nov 8 2022 Xiong Zhou <xiongzhou4@huawei.com> - 1.0.4-4
- Delete the la files and add secure flags for mathlib and jemalloc.

* Mon Nov 7 2022 Xiong Zhou <xiongzhou4@huawei.com> - 1.0.3-3
- Align OpenSSL build command with that of the OpenSSL repository on openEuler community.

* Wed Nov 2 2022 Xiong Zhou <xiongzhou4@huawei.com> - 1.0.2-2
- Fix the bug that libcrypto.so cannot be found.

* Tue Sep 20 2022 Xiong Zhou <xiongzhou4@huawei.com> - 1.0.0-1
- Init gcc-for-openEuler repository.
